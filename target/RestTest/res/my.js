function hide_all() {
    $("#getById").hide();
    $("#delById").hide();
    $("#newUser").hide();
    $("#editUser").hide();
    $("#getAll").hide();
};
$(document).ready(function () {
    hide_all();
    $("#get_button").click(function () {
        hide_all();
        $("#getById").show();
        $('#byIdResponse').empty();
    });
    $("#del_button").click(function () {
        hide_all();
        $('#delpersonIdResponse').empty();
        $("#delById").show();
    });
    $("#new_button").click(function () {
        hide_all();
        $('#personFormResponse').empty();
        $("#newUser").show();
    });
    $("#edit_button").click(function () {
        hide_all();
        $('#editpersonFormResponse').empty();
        $("#editUser").show();
    });

    $('#byId').submit(function (e) {
        var personId = +$('#personId').val();
        $.ajax({
            type: 'GET',
            url: '/user/' + personId,
            contentType: 'text/html; charset=utf-8',
            statusCode: {
                200: function (user) {
                    $('#byIdResponse').html('User name ' + user.name + '</br>Age ' + user.age);
                },
                404: function () {
                    $('#byIdResponse').text("User with id " + personId + " doesn't exist.");
                }
            },
            success: function () {
                $('#byIdResponse').text("Default code");
            }
        });
        $("#byId").trigger("reset");
        e.preventDefault();
    });

    $('#del').submit(function (e) {
        var personId = +$('#delpersonId').val();
        $.ajax({
            type: 'DELETE',
            url: '/user/' + personId,
            contentType: 'text/html; charset=utf-8',
            statusCode: {
                204: function () {
                    $('#delpersonIdResponse').text("User with id: " + personId + " successfully deleted");
                },
                404: function () {
                    $('#delpersonIdResponse').text("User with id " + personId + " doesn't exist.");
                }
            },
            success: function () {
                $('#delpersonIdResponse').text("def");
            }
        });
        $("#del").trigger("reset");
        e.preventDefault();
    });

    $('#edit').submit(function (e) {
        var personId = +$('#editId').val();
        var data = {'name': $('#editnameInput').val(), 'age': $('#editageInput').val()};
        $.ajax({
            type: 'PUT',
            url: '/user/' + personId,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            statusCode: {
                200: function (user) {
                    $('#editpersonFormResponse').html("User updated! </br> New name " + user.name
                        + "</br>New Age " + user.age);
                },
                404: function () {
                    $('#editpersonFormResponse').text("User with id " + personId + " doesn't exist.");
                }
            },
            success: function () {
                $('#editpersonFormResponse').text("def");
            }
        });
        $("#edit").trigger("reset");
        e.preventDefault();
    });

    $('#newPersonForm').submit(function (e) {
        var data = {'name': $('#nameInput').val(), 'age': $('#ageInput').val()};
        $.ajax({
            type: 'POST',
            url: '/user/',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            statusCode: {
                201: function (user) {
                    $('#personFormResponse').html("User created! id: " + user.id);
                },
                409: function () {
                    $('#personFormResponse').text("User with name " + $('#nameInput').val() + " already exist.");
                }
            },
            success: function () {
                $('#personFormResponse').text('def');
            }
        });
        $("#newPersonForm").trigger("reset");
        e.preventDefault();
    });

    $('#all_button').click(function (e) {
        hide_all();
        $("#getAll").show();
        $.ajax({
            type: 'GET',
            url: '/user/',
            success: function (response) {
                var answer = '';
                $.each(response, function (index, el) {
                    answer = answer + ' name ' + el.name + ' age ' + el.age;
                });
                $('#allAnswer').text(answer);
            }
        });
        e.preventDefault();
    });
});