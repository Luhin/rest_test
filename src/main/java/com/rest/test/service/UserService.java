package com.rest.test.service;

import com.rest.test.dao.BaseDAO;
import com.rest.test.dao.HibernateUserDAO;
import com.rest.test.entity.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Владимир on 06.01.2017.
 */
@Service
public class UserService {
    @Inject
    @Qualifier("jpa")
    private BaseDAO userDAO;

    public boolean saveUser(User user) {
        userDAO.save(user);
        return true;
    }

    public User getUser(Long id) {
        return userDAO.get(id);
    }

    public User updateUser(Long id, User user) {
        User persist = userDAO.get(id);
        if (persist != null) {
            persist.setName(user.getName());
            persist.setAge(user.getAge());
            userDAO.update(persist);
        }
        return persist;
    }

    public List<User> getAllUsers() {
        return userDAO.getAll();
    }

    public boolean removeUser(Long id) {
        User user = userDAO.get(id);
        if (user == null) {
            return false;
        }
        userDAO.remove(user);
        return true;
    }
}
