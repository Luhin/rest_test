package com.rest.test.dao;

import com.rest.test.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by Владимир on 28.12.2016.
 */
@Repository
@Qualifier("hib")
@Transactional(value = "hibernateTransactionManager")
public class HibernateUserDAO implements BaseDAO{
    @Inject
    private SessionFactory sessionFactory;

    @Transactional(value = "hibernateTransactionManager", propagation = Propagation.REQUIRES_NEW)
    public void save(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.save(user);
    }

    @Transactional(value = "hibernateTransactionManager", propagation = Propagation.REQUIRES_NEW)
    public void update(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.merge(user);
    }

    @Transactional(value = "hibernateTransactionManager", propagation = Propagation.SUPPORTS, readOnly = true)
    public User get(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(User.class, id);
    }

    @Transactional(value = "hibernateTransactionManager", propagation = Propagation.SUPPORTS, readOnly = true)
    public List<User> getAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(User.class).list();
    }

    @Transactional(value = "hibernateTransactionManager", propagation = Propagation.REQUIRES_NEW)
    public void remove(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.remove(user);
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}
