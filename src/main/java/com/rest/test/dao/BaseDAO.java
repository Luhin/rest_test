package com.rest.test.dao;

import com.rest.test.entity.User;

import java.util.List;

/**
 * Created by Владимир on 12.01.2017.
 */
public interface BaseDAO {
    void save(User user);

    void update(User user);

    User get(Long id);

    List<User> getAll();

    void remove(User user);
}
