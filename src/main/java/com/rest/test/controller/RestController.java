package com.rest.test.controller;


import com.rest.test.entity.User;
import com.rest.test.dao.HibernateUserDAO;
import com.rest.test.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Владимир on 28.12.2016.
 */

@org.springframework.web.bind.annotation.RestController
public class RestController {
    @Inject
    private UserService service;

    @RequestMapping(value = "/user/", method = RequestMethod.GET)
    public ResponseEntity<List<User>> allUsers() {
        System.out.println("Get ALL users ===========================");
        List<User> list = service.getAllUsers();
        if (list.isEmpty()) {
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<User>>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET
            , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUserById(@PathVariable long id) {
        System.out.println("Trying to find user with id " + id + "==============");
        User user = service.getUser(id);
        if (user == null) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/", method = RequestMethod.POST)
    public ResponseEntity<User> createUser(@RequestBody User user, UriComponentsBuilder uBuilder) {
        System.out.println("Creating user " + user.getName() + "==============");
        //is exists method
        if (service.saveUser(user)) {
            System.out.println(user.getId() + "id ========================");
            return new ResponseEntity<User>(user, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<User>(user, HttpStatus.CONFLICT);// this name already exists
        }
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable long id, @RequestBody User user) {
        System.out.println("Updating user " + user.getName() + "==============");
        User result = service.updateUser(id, user);
        if (result == null) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUser(@PathVariable long id) {
        System.out.println("delete user with id " + id + "===========");
        if (!service.removeUser(id)) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

}
