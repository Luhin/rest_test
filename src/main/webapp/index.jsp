<html>
<head>
    <title>My REST</title>
</head>
<body background="res/bg.png">
<script src="/res/jquery-3.1.1.js"></script>
<script src="/res/my.js"></script>
<div id="content">
    <h2>Hello REST!</h2>
    <div id="menu">
        <input id="get_button" type="button" value="Get user"/>
        <input id="del_button" type="button" value="Delete user"/>
        <input id="new_button" type="button" value="Create user"/>
        <input id="edit_button" type="button" value="Edit user"/>
        <input id="all_button" type="button" value="All users"/>
    </div>
    <div id="getById">
        <h2>Get User By ID</h2>
        <form id="byId">
            <label for="personId">ID : </label>
            <input name="id" id="personId" value="0" type="text"/>
            <input type="submit" value="Get user"/>
            <br/>
            <br/>
            <div id="byIdResponse"></div>
        </form>
    </div>
    <div id="delById">
        <h2>Delete By ID</h2>
        <form id="del">
            <label for="delpersonId">ID : </label>
            <input name="id" id="delpersonId" value="0" type="number"/>
            <input type="submit" value="Delete user"/>
            <br/>
            <br/>
            <div id="delpersonIdResponse"></div>
        </form>
    </div>
    <div id="newUser">
        <h2>Submit new Person</h2>
        <form id="newPersonForm">
            <label for="nameInput">Name: </label>
            <input type="text" name="name" id="nameInput"/>
            <br/>
            <label for="ageInput">Age: </label>
            <input type="text" name="age" id="ageInput"/>
            <br/>
            <input type="submit" value="Save user"/><br/><br/>
            <div id="personFormResponse" class="green"></div>
        </form>
    </div>
    <div id="editUser">
        <h2>Edit Person</h2>
        <form id="edit">
            <label for="editId">ID: </label>
            <input type="number" name="age" id="editId" value="0"/>
            <br/>
            <label for="editnameInput">Name: </label>
            <input type="text" name="name" id="editnameInput"/>
            <br/>
            <label for="editageInput">Age: </label>
            <input type="text" name="age" id="editageInput"/>
            <br/>
            <input type="submit" value="Edit user"/>
            <br/>
            <br/>
            <div id="editpersonFormResponse" class="green"></div>
        </form>
    </div>
    <div id="getAll">
        <div id="allAnswer"></div>
    </div>
</div>
</body>
</html>
