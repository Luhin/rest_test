package com.rest.test.testing;

import com.rest.test.configuration.AppInitializer;
import com.rest.test.configuration.Configuration;
import com.rest.test.dao.JPAUserDAO;
import com.rest.test.entity.User;
import com.rest.test.dao.HibernateUserDAO;
import com.rest.test.service.UserService;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.inject.Inject;

/**
 * Created by Владимир on 28.12.2016.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Configuration.class, AppInitializer.class})
public class Test {
    @Inject
    private UserService service;

    @org.junit.Test
    public void getTest() {
        User user = service.getUser(7L);
        System.out.println(user.getName());
    }

    @org.junit.Test
    public void removeTest(){
       service.removeUser(22L);
    }
}
